import sqlite3
from sqlite3 import Error
from os import system, name
import os


def limpiaPantalla():
    system('cls')

def imprimirMensaje(mensaje=False):
    if(mensaje):
        if mensaje[0] == ALERTA:
            print(F"\033[93m{mensaje[1]}\033[0m")
        elif mensaje[0] == EXITO:
            print(f"\033[94m{mensaje[1]}\033[0m")

def crear_conexion(db_file):
    global conexion
    """create a database connection to a SQLITE database"""   
    try:
        conexion = sqlite3.connect(db_file)        
    except Error as e:
        print(e)
    return conexion
   
def crear_tabla(conexion, crear_tabla_sqlite):
    try:
        c = conexion.cursor()
        c.execute(crear_tabla_sqlite)
    except Error as e:
        print(e)

def crear_tablas(conexion=False):
    sql_crear_tabla_projectos = """CREATE TABLE IF NOT EXISTS proyectos(
                                    id integer PRIMARY KEY AUTOINCREMENT,
                                    nombre text NOT NULL,
                                    fecha_de_inicio text,
                                    fecha_de_termino text                              
                                   );"""

    slq_crear_tabla_tareas = """CREATE TABLE IF NOT EXISTS tareas(
                                    id integer PRIMARY KEY AUTOINCREMENT,
                                    nombre text NOT NULL,
                                    prioridad integer,
                                    estado_id integer NOT NULL,
                                    projecto_id integer NOT NULL,
                                    fecha_de_inicio text NOT NULL,
                                    fecha_de_termino text NOT NULL,
                                    FOREIGN KEY (projecto_id) REFERENCES projectos (id)
                                );"""
    if conexion is not None: 
        #crear tabla projectos       
        crear_tabla(conexion, sql_crear_tabla_projectos)
        #crear tabla tareas
        crear_tabla(conexion, slq_crear_tabla_tareas)
    else:
        print("¡Error! No se pudo conectar a la base de datos")

def nuevaTarea(proyecto_id, mensaje=False):
    limpiaPantalla()   
    cursor = conexion.cursor()
    sql= ""
    valores = None    
    print("AGREGA LOS DATOS DE LA TAREA ")
    print("")
    imprimirMensaje(mensaje)
    nombre = input("Nombre de la tarea: ")
    cursor.execute(f"SELECT * FROM tareas WHERE nombre='{nombre}'")
    tarea = cursor.fetchone()
    if tarea:
        nuevaTarea(proyecto_id, [ALERTA, f"Ya existe una tarea con el nombre {nombre}"])
        return False
    prioridad = input("Ingresa la prioridad: ")
    estado_id = input("Ingresa el estado: ")
    fecha_de_inicio = input("Fecha de inicio: ")
    fecha_de_termino = input("Fecha de término: ")
    sql = "INSERT INTO tareas VALUES(?,?,?,?,?,?,?)"
    valores = (None, nombre, prioridad, estado_id, proyecto_id, fecha_de_inicio, fecha_de_termino)
    cursor.execute(sql, valores)
    conexion.commit()
    verProyecto(proyecto_id, [EXITO,"La tarea se ha creado correctamente"])

def borrarTarea(proyecto_id, tarea_id, nombre_tarea):
    resp = input(f"\033[93m¿Estás seguro que deseas eliminar la Tarea {nombre_tarea}? (si/no) \033[0m") 
    if resp=="si":
        cursor = conexion.cursor()
        sqlite = f"DELETE FROM tareas WHERE id = {tarea_id}"
        cursor.execute(sqlite)
        conexion.commit()
        verProyecto(proyecto_id, [EXITO,f"La Tarea {nombre_tarea} fué eliminada correctamente"])
    else:
        verTarea(proyecto_id, tarea_id)

def editarTarea(proyecto_id, tarea_id, nombre_tarea, mensaje=False):
    limpiaPantalla()   
    cursor = conexion.cursor()
    print(f"Actualizar la tarea {nombre_tarea}")
    print("")
    imprimirMensaje(mensaje)
    nombre = input("Nombre de la tarea: ")
    cursor.execute(f"SELECT * FROM tareas WHERE nombre='{nombre}'")
    tarea = cursor.fetchone()
    if tarea and tarea[1]!=nombre_tarea:
        editarTarea(proyecto_id, tarea_id, nombre_tarea, [ALERTA, f"Ya existe una tarea con el nombre {nombre}"])
        return False
    prioridad = input("Ingresa la prioridad: ")
    estado_id = input("Ingresa el estado: ")
    fecha_de_inicio = input("Fecha de inicio: ")
    fecha_de_termino = input("Fecha de término: ")
    sql = """UPDATE tareas
                SET nombre = ?,
                prioridad = ?,
                estado_id=?,
                projecto_id=?,
                fecha_de_inicio = ?,
                fecha_de_termino = ?
                WHERE id = ?"""   
    valores = (nombre, prioridad, estado_id, proyecto_id, fecha_de_inicio, fecha_de_termino, tarea_id)
    cursor.execute(sql, valores)    
    conexion.commit()
    verProyecto(proyecto_id, [EXITO,f"La tarea {nombre} se ha editado correctamente"])

def verTareas(proyecto_id):
    cursor = conexion.cursor()
    sqlite = f"SELECT * FROM tareas WHERE projecto_id = {proyecto_id}"
    cursor.execute(sqlite)
    tareas = cursor.fetchall()
    if len(tareas) == 0:
        print("No hay tareas existentes en este proyecto")
    for tarea in tareas:
        print(f"{tarea[0]}-{tarea[1]}")
    cursor.close()

def verTarea(proyecto_id, tarea_id, mensaje=False):
    limpiaPantalla()
    cursor = conexion.cursor()
    sqlite = f"SELECT * FROM proyectos WHERE id={proyecto_id}"
    cursor.execute(sqlite)
    proyecto = cursor.fetchone()

    cursor = conexion.cursor()
    sqlite = f"SELECT * FROM tareas WHERE id={tarea_id} AND projecto_id={proyecto_id}"
    cursor.execute(sqlite)
    tarea = cursor.fetchone()
    if(tarea):
        print(f"TAREA {tarea[1].upper()}")
        print("")
        print(f"Prioridad: {tarea[2]}")
        print(f"Estado: {tarea[3]}")
        print(f"Proyecto: {proyecto[1]}")

        print("")
        print(f"fecha de inicio: {tarea[5]}")
        print(f"fecha de cierre: {tarea[6]}")
        print("---------------------------------------------")
        print("editar")
        print("borrar")
        print("regresar")
        print("---------------------------------------------")
        imprimirMensaje(mensaje)
        elemento = input("¿Que opción desea? ") 
        if elemento == "editar":
            editarTarea(proyecto_id, tarea_id, tarea[1])
        elif elemento == "borrar":
            borrarTarea(proyecto_id, tarea_id, tarea[1])
        elif elemento == "regresar":
            verProyecto(proyecto_id)
        else:
            verTarea(proyecto_id, tarea_id, [ALERTA,"Error al introducir los datos"])
    else:
        verProyecto(proyecto_id, [ALERTA,f"La tarea no existe en el proyecto {proyecto[1]}"])

def nuevoProyecto(mensaje=False):
    limpiaPantalla()   
    cursor = conexion.cursor()
    print(f"AGREGA LOS DATOS DEL PROYECTO")
    print("")
    imprimirMensaje(mensaje)
    nombre = input("Nombre del proyecto: ")
    cursor.execute(f"SELECT * FROM proyectos WHERE nombre='{nombre}'")
    proyecto = cursor.fetchone()
    if proyecto:
        nuevoProyecto([ALERTA, f"Ya existe un proyecto con el nombre {nombre}"])
        return False
    fecha_de_inicio = input("Fecha de inicio: ")
    fecha_de_termino = input("Fecha de término: ")
    sql = """INSERT INTO proyectos(nombre, fecha_de_inicio, fecha_de_termino)
             VALUES(?,?,?)"""
    valores = (nombre, fecha_de_inicio, fecha_de_termino)
    cursor.execute(sql, valores)
    conexion.commit()
    menu_principal([EXITO,f"El proyecto {nombre} se ha creado correctamente"])

def borrarProyecto(proyecto_id, nombre_proyecto):
    print("")
    print(f"Se eliminarán todas las tareas al proyecto {nombre_proyecto}")
    resp = input(f"\033[93m¿Estás seguro que deseas eliminar el proyecto {nombre_proyecto}? (si/no) \033[0m") 
    if resp=="si":
        cursor = conexion.cursor()
        sqlite = f"DELETE FROM tareas WHERE projecto_id = {proyecto_id}"
        cursor.execute(sqlite)
        conexion.commit()
        sqlite = f"DELETE FROM proyectos WHERE id = {proyecto_id}"
        cursor.execute(sqlite)
        conexion.commit()
        menu_principal([EXITO,f"El proyecto {nombre_proyecto} fué eliminado correctamente"])
    else:
        verProyecto(proyecto_id)

def editarProyecto(proyecto_id, nombre_proyecto, mensaje=False):
    limpiaPantalla()   
    cursor = conexion.cursor()
    print(f"Actualizar el proyecto {nombre_proyecto}")
    print("")
    imprimirMensaje(mensaje)
    nombre = input("Nombre del proyecto: ")
    cursor.execute(f"SELECT * FROM proyectos WHERE nombre='{nombre}'")
    proyecto = cursor.fetchone()
    if proyecto and proyecto[1]!=nombre_proyecto:
        editarProyecto(proyecto_id, nombre_proyecto, [ALERTA, f"Ya existe un proyecto con el nombre {nombre}"])
        return False
    fecha_de_inicio = input("Fecha de inicio: ")
    fecha_de_termino = input("Fecha de término: ")
    sql = """UPDATE proyectos
                SET nombre = ?,
                fecha_de_inicio = ?,
                fecha_de_termino = ?
                WHERE id = ?"""   
    valores = (nombre, fecha_de_inicio, fecha_de_termino, proyecto_id)
    cursor.execute(sql, valores)    
    conexion.commit()
    menu_principal([EXITO,f"El proyecto {nombre} se ha editado correctamente"])

def verProjectos():    
    try:        
        cursor = conexion.cursor()
        sqlite = "SELECT * FROM proyectos"
        cursor.execute(sqlite)
        projectos = cursor.fetchall()
        if len(projectos) == 0:
            print("No hay proyectos existentes")
        for projecto in projectos:
            print(f"{projecto[0]}-{projecto[1]}")
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)

def verProyecto(id, mensaje=False):
    limpiaPantalla()
    cursor = conexion.cursor()
    sqlite = f"SELECT * FROM proyectos WHERE id={id}"
    cursor.execute(sqlite)
    proyecto = cursor.fetchone()
    if(proyecto):
        print(f"PROYECTO {proyecto[1].upper()}")
        print("")
        print(f"fecha de inicio: {proyecto[2]}")
        print(f"fecha de cierre: {proyecto[3]}")
        print("")
        print("Tareas asociadas al proyecto:")
        print("---------------------------------------------")
        verTareas(id)
        print("nueva")
        print("---------------------------------------------")        
        print("editar")
        print("borrar")
        print("regresar")
        print("---------------------------------------------")
        imprimirMensaje(mensaje)
        elemento = input("¿Que opción desea? ") 
        if elemento.isdigit():
            verTarea(id, int(elemento))
        elif elemento == "nueva":
            nuevaTarea(proyecto[0])
        elif elemento == "editar":
            editarProyecto(id, proyecto[1])
        elif elemento == "borrar":
            borrarProyecto(id, proyecto[1])
        elif elemento == "regresar":
            menu_principal()  
        else:
            verProyecto(id, [ALERTA,"Error al introducir los datos"])
    else:
        menu_principal([ALERTA,"El proyecto no existe"])

def menu_principal(mensaje=False):
    limpiaPantalla()
    baseDeDatos = r"C:\sqlite\db\pythonsqlite_es.db"
    # Crear conexión    
    conexion = crear_conexion(baseDeDatos)
    crear_tablas(conexion)
    print("PROYECTOS")    
    print("")
    print("Eliga un proyecto")
    print("---------------------------------------------")
    verProjectos()
    print("---------------------------------------------")
    print("nuevo")
    print("salir")
    print("---------------------------------------------")
    imprimirMensaje(mensaje)
    elemento = input("¿Que opción desea? ") 
    if elemento.isdigit():
        verProyecto(int(elemento))
    elif elemento == "nuevo":
        nuevoProyecto()   
    elif elemento == "salir":
        return  
    else:
        menu_principal([ALERTA,"Error al introducir los datos"])

conexion = None
ALERTA = 1
EXITO = 2
menu_principal()

